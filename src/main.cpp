#include <iostream>

#include "Game.hpp"
#include "ResourceHolder.hpp"

#include "Resource.hpp"

int main()
{
  Game game;

  return game.run();
}
