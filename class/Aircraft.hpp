#ifndef AIRCRAFT_HPP_
# define AIRCRAFT_HPP_

# include <SFML/Graphics.hpp>

# include "ResourceHolder.hpp"
# include "Entity.hpp"

# include "Resource.hpp"

class Aircraft : public Entity
{
public:
  enum Type
    {
      Eagle,
      Raptor
    };

public:
  explicit Aircraft(Type type, const TextureHolder & textures);
  Aircraft(const Aircraft & o);
  Aircraft & operator=(const Aircraft & o);
  virtual ~Aircraft();

protected:
  virtual void		drawCurrent(sf::RenderTarget & target, sf::RenderStates states) const;

private:
  Type			_type;
  sf::Sprite		_sprite;
};

#endif // !AIRCRAFT_HPP_
