#include "Scroll.hpp"

Scroll::Scroll(sf::Vector2f & follow, sf::RenderWindow & win)
  : _limit(0, 0, 1, 1)
  , _size(1, 1)
  , _follow(follow)
  , _win(win)
{

}

Scroll::Scroll(const Scroll & o)
  : _limit(o._limit)
  , _size(o._size)
  , _follow(o._follow)
  , _win(o._win)
{

}

Scroll & Scroll::operator=(const Scroll & o)
{
  if (&o == this)
    return *this;
  _limit = o._limit;
  _size = o._size;
  _follow = o._follow;
  return *this;
}

Scroll::~Scroll()
{

}

void Scroll::setLimit(const sf::IntRect & limit)
{
  _limit = limit;
}

void Scroll::setSize(const sf::Vector2u & size)
{
  _size = size;
}

void Scroll::changeFollow(const sf::Vector2f & follow)
{
  _follow = follow;
}

const sf::IntRect & Scroll::getLimit() const
{
  return _limit;
}

const sf::Vector2u & Scroll::getSize() const
{
  return _size;
}

const sf::Vector2f & Scroll::getFollow() const
{
  return _follow;
}

void Scroll::update()
{
  sf::Vector2f center(_follow);

  if (_follow.x - (_size.x / 2) < _limit.left)
    center.x = _size.x / 2;
  else if (_follow.x + (_size.x / 2) >= (_limit.left + _limit.width))
    center.x = _limit.left - (_size.x / 2);
  if (_follow.y - (_size.y / 2) < _limit.top)
    center.y = _size.y / 2;
  else if (_follow.y + (_size.y / 2) >= (_limit.top + _limit.height))
    center.y = _limit.top - (_size.y / 2);
  _win.setView(sf::View(center, sf::Vector2f(_size)));
}
