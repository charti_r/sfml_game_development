#include "Entity.hpp"

Entity::Entity()
  : SceneNode()
  , _velocity(0.f, 0.f)
{

}

Entity::~Entity()
{

}

void			Entity::setVelocity(const sf::Vector2f & velocity)
{
  _velocity = velocity;
}

void			Entity::setVelocity(const float x, const float y)
{
  _velocity = sf::Vector2f(x, y);
}

const sf::Vector2f &	Entity::getVelocity() const
{
  return _velocity;
}

void			Entity::updateCurrent(const sf::Time & deltaTime)
{
  Transformable::move(_velocity * deltaTime.asSeconds());
}
