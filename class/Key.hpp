#ifndef KEY_HPP_
# define KEY_HPP_

# include <SFML/Window.hpp>

class Key
{
public:
  Key();
  Key(const Key & o);
  Key & operator=(const Key & o);
  virtual ~Key();

  void	update(const sf::Event & event);
  bool	isDown(sf::Keyboard::Key key) const;

protected:
  std::vector<bool>	_down;
};

#endif // !KEY_HPP_
