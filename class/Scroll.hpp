#ifndef SCROLL_HPP_
# define SCROLL_HPP_

# include <SFML/Window.hpp>
# include <SFML/Graphics.hpp>

class Scroll
{
public:
  explicit Scroll(sf::Vector2f & follow, sf::RenderWindow & win);
  Scroll(const Scroll & o);
  Scroll & operator=(const Scroll & o);
  virtual ~Scroll();

  void			setLimit(const sf::IntRect & limit);
  void			setSize(const sf::Vector2u & size);
  void			changeFollow(const sf::Vector2f & follow);

  const sf::IntRect &	getLimit() const;
  const sf::Vector2u &	getSize() const;
  const sf::Vector2f &	getFollow() const;

  virtual void		update();

protected:
  sf::IntRect		_limit;
  sf::Vector2u		_size;
  sf::Vector2f &	_follow;
  sf::RenderWindow &	_win;
};

#endif // !SCROLL_HPP_
