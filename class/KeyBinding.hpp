#ifndef KEYBINDING_HPP_
# define KEYBINDING_HPP_

# include <fstream>
# include <vector>
# include <string>
# include <map>

# include <SFML/Window.hpp>

class KeyBinding
{
public:
  explicit KeyBinding(unsigned int size = 0);
  KeyBinding(const KeyBinding & o);
  KeyBinding & operator=(const KeyBinding & o);
  virtual ~KeyBinding();

  // syntax "action=key # comment"
  bool	loadFromFile(const std::string & filename, std::map<std::string, unsigned int> & bind);

  std::vector<sf::Keyboard::Key> &		operator()();
  sf::Keyboard::Key &				operator[](unsigned int action);

  const std::vector<sf::Keyboard::Key> &	operator()() const;
  const sf::Keyboard::Key &			operator[](unsigned int action) const;

protected:
  std::vector<sf::Keyboard::Key>			_bind;

private:
  static std::map<std::string, sf::Keyboard::Key>	_sfbind;
};

#endif // !KEYBINDING_HPP_
