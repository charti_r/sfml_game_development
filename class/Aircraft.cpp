#include "Aircraft.hpp"

static Resource::Texture::ID	toTextureID(Aircraft::Type type)
{
  switch (type)
    {
    case Aircraft::Eagle:
      return Resource::Texture::Eagle;
      break;
    case Aircraft::Raptor:
      return Resource::Texture::Raptor;
      break;
    default:
      return Resource::Texture::None;
      break;
    }
  return Resource::Texture::None;
}

Aircraft::Aircraft(Aircraft::Type type, const TextureHolder & textures)
  : Entity()
  , _type(type)
  , _sprite(textures[toTextureID(type)])
{
  sf::FloatRect bounds;

  // Set sprite origin on its center
  bounds = _sprite.getLocalBounds();
  _sprite.setOrigin(bounds.width / 2.f, bounds.height / 2.f);
}

Aircraft::~Aircraft()
{

}

void			Aircraft::drawCurrent(sf::RenderTarget & target, sf::RenderStates states) const
{
  target.draw(_sprite, states);
}
