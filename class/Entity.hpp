#ifndef ENTITY_HPP_
# define ENTITY_HPP_

# include <SFML/System.hpp>

# include "SceneNode.hpp"

class Entity : public SceneNode
{
public:
  Entity();
  Entity(const Entity & o);
  Entity & operator=(const Entity & o);
  virtual ~Entity();

  void			setVelocity(const sf::Vector2f & velocity);
  void			setVelocity(const float x, const float y);
  const sf::Vector2f &	getVelocity() const;

protected:
  virtual void		updateCurrent(const sf::Time & deltaTime);

protected:
  sf::Vector2f		_velocity;
};

#endif // !ENTITY_HPP_
