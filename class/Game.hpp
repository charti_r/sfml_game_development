#ifndef GAME_HPP_
# define GAME_HPP_

# include <SFML/Window.hpp>
# include <SFML/Graphics.hpp>

namespace KeyFlag
{
  enum Code
    {
      None	= 0,
      Up	= 1 << 0,
      Left	= 1 << 1,
      Down	= 1 << 2,
      Right	= 1 << 3
    };
}

class Game
{
public:
  Game();
  Game(const Game & o);
  Game & operator=(const Game & o);
  virtual ~Game();

  int			run();

private:
  void			processEvents();
  void			update(const sf::Time & deltaTime);
  void			render();

  void			playerInputKeyPressed(sf::Keyboard::Key key);
  void			playerInputKeyReleased(sf::Keyboard::Key key);
  void			gameOver();

private:
  sf::RenderWindow	_win;
  sf::Texture		_texture;
  sf::Sprite		_player;

  bool			_error;

  const sf::Time	_timePerFrame;
  const float		_playerSpeed;
  unsigned int		_keyFlag;
};

#endif // !GAME_HPP_
