#include "Key.hpp"

Key::Key()
  : _down(sf::Keyboard::KeyCount, false)
{

}

Key::Key(const Key & o)
  : _down(o._down)
{

}

Key & Key::operator=(const Key & o)
{
  if (&o == this)
    return *this;
  _down = o._down;
  return *this;

}

Key::~Key()
{

}

void Key::update(const sf::Event & event)
{
  switch (event.type)
    {
    case sf::Event::KeyPressed:
      _down[event.key.code] = true;
      break;
    case sf::Event::KeyReleased:
      _down[event.key.code] = false;
      break;
    default:
      break;
    }
}

bool Key::isDown(sf::Keyboard::Key key) const
{
  return _down[key];
}
