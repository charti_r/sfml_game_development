#include "Game.hpp"

Game::Game()
  : _win(sf::VideoMode(640, 480), "SFML Game Devlopment")
  , _texture()
  , _player()
  , _error(false)
  , _timePerFrame(sf::seconds(1.f / 60.f))
  , _playerSpeed(50.f)
  , _keyFlag(KeyFlag::None)
{
  if (_texture.loadFromFile("rsc/img/player.png"))
    {
      _player.setTexture(_texture);
      _player.setPosition(100.f, 100.f);
    }
  else
    _error = true;
}

Game::~Game()
{

}

int			Game::run()
{
  sf::Clock		clock;
  sf::Time		deltaTime;

  deltaTime = sf::Time::Zero;
  while (!_error && _win.isOpen())
    {
      processEvents();
      deltaTime += clock.restart();
      while (deltaTime >= _timePerFrame)
	{
	  deltaTime -= _timePerFrame;
	  processEvents();
	  update(_timePerFrame);
	}
      render();
    }
  return 0;
}

void			Game::processEvents()
{
  sf::Event		event;

  while (_win.pollEvent(event))
    {
      switch (event.type)
	{
	case sf::Event::Closed:
	  gameOver();
	  break;
	case sf::Event::KeyPressed:
	  playerInputKeyPressed(event.key.code);
	  break;
	case sf::Event::KeyReleased:
	  switch (event.key.code)
	    {
	    case sf::Keyboard::Escape:
	      gameOver();
	      break;
	    default:
	      playerInputKeyReleased(event.key.code);
	      break;
	    }
	  break;
	default:
	  break;
	}
    }
}

void			Game::update(const sf::Time & deltaTime)
{
  sf::Vector2f		movement(0.f, 0.f);

  if (_keyFlag & KeyFlag::Up)
    movement.y -= _playerSpeed;
  if (_keyFlag & KeyFlag::Left)
    movement.x -= _playerSpeed;
  if (_keyFlag & KeyFlag::Down)
    movement.y += _playerSpeed;
  if (_keyFlag & KeyFlag::Right)
    movement.x += _playerSpeed;

  _player.move(movement * deltaTime.asSeconds());
}

void			Game::render()
{
  _win.clear();
  _win.draw(_player);
  _win.display();
}

void			Game::playerInputKeyPressed(sf::Keyboard::Key key)
{
  switch (key)
    {
    case sf::Keyboard::W:
    case sf::Keyboard::Up:
      _keyFlag |= KeyFlag::Up;
      break;
    case sf::Keyboard::A:
    case sf::Keyboard::Left:
      _keyFlag |= KeyFlag::Left;
      break;
    case sf::Keyboard::S:
    case sf::Keyboard::Down:
      _keyFlag |= KeyFlag::Down;
      break;
    case sf::Keyboard::D:
    case sf::Keyboard::Right:
      _keyFlag |= KeyFlag::Right;
      break;
    default:
      break;
    }
}

void			Game::playerInputKeyReleased(sf::Keyboard::Key key)
{
  switch (key)
    {
    case sf::Keyboard::W:
    case sf::Keyboard::Up:
      _keyFlag &= ~KeyFlag::Up;
      break;
    case sf::Keyboard::A:
    case sf::Keyboard::Left:
      _keyFlag &= ~KeyFlag::Left;
      break;
    case sf::Keyboard::S:
    case sf::Keyboard::Down:
      _keyFlag &= ~KeyFlag::Down;
      break;
    case sf::Keyboard::D:
    case sf::Keyboard::Right:
      _keyFlag &= ~KeyFlag::Right;
      break;
    default:
      break;
    }
}

void			Game::gameOver()
{
  _win.close();
}
