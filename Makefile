CPPFLAGS	+= -W -Wall -Wextra -Werror
CPPFLAGS	+= -I ./inc -I ./class

LIB		+= -lsfml-system -lsfml-window -lsfml-graphics -lsfml-audio

NAME		= GameDev

SRC		= \
		src/main.cpp \
		class/Game.cpp \
		class/SceneNode.cpp \
		class/Key.cpp \
		class/KeyBinding.cpp \
		class/Entity.cpp \
		class/Aircraft.cpp

OBJ		= $(SRC:.cpp=.o)

all: $(NAME)

$(NAME): $(OBJ)
	g++ $(OBJ) $(LIB) -o $(NAME)

clean:
	rm -f $(OBJ)

fclean: clean
	rm -f $(NAME)

re: fclean all

.PHONY: all clean fclean re
